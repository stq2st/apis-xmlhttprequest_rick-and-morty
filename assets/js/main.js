/*function buscarPersonaje(url, callback) {
    let xhttp;
    xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let personaje = JSON.parse(this.responseText);
            callback(personaje.name);
        } else {
            const error = new Error('Error ' + url);
            return callback(error, null);
        }

    };
    xhttp.open("GET", url, true);
    xhttp.send();

}*/

const urlBase = "https://rickandmortyapi.com/api/character";

var xhttp = new XMLHttpRequest();
xhttp.open('GET', urlBase, true)
xhttp.send()
xhttp.onload = function() {
    if (this.readyState == 4 && this.status == 200) {
        var personajes = JSON.parse(xhttp.responseText);

        var infoDePersonaje = personajes.results
        window.localStorage.setItem("personajes", JSON.stringify(infoDePersonaje))
        var listaPersonajes = localStorage.getItem('personajes')
        var todosLosPersonajes = JSON.parse(listaPersonajes)
        console.log(todosLosPersonajes)
        mostrarPersonajes(todosLosPersonajes)
    } else {
        console.log("Error de tipo:" + xhttp.status)
    }
}

function mostrarPersonajes(todosLosPersonajes) {
    var mostrarPersonaje = document.getElementById('Lista-personajes');
    todosLosPersonajes.forEach(personaje => {
        var div1 = document.createElement('div')
        div1.setAttribute('class', 'card');
        div1.style.borderRadius = "25px"

        var cartasDePersonajes = document.createElement('img')
        div1.setAttribute('class', 'col-4')
        cartasDePersonajes.setAttribute("src", personaje.image);
        cartasDePersonajes.style.height = "200px"
        cartasDePersonajes.style.width = "200px"
        cartasDePersonajes.style.borderRadius = "50px"
        cartasDePersonajes.style.border = "7px solid lightblue"
        cartasDePersonajes.style.boxShadow = "10px 10px gray"

        div1.appendChild(cartasDePersonajes);
        var div2 = document.createElement('div')
        var h5 = document.createElement('h5')
        var datos = document.createElement('p')
        div2.innerHTML = `Nombre:` + personaje.name
        h5.innerHTML = `Genero:` + personaje.gender
        datos.innerHTML = `Especie:` + personaje.species
        cartasDePersonajes.setAttribute('class', 'card-img-top');
        div2.setAttribute('class', 'card-body')
        h5.setAttribute('class', 'card-title');
        datos.setAttribute('class', 'card-text');
        div1.appendChild(div2);
        div2.appendChild(h5);
        div2.appendChild(datos);
        mostrarPersonaje.appendChild(div1);
    })
}